Jawaban 1 Membuat Database
create database myshop;

Jawaban 2 Membuat Table di dalam Database

Table users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories
 create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> users_id int(8),
    -> foreign key(users_id) references users(id)
    -> );

create table items(
    -> id int(9) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> categories_id int(8),
    -> foreign key(categories_id) references categories(id)
    -> );

Jawaban 3 Memasukkan data 
Table users
insert into users(name,email,password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jane123");

Table categories
insert into categories(name,users_id) values("gadjet",1),("cloth",2),("men",1),("women",2),("branded",2);

Table items
insert into items(name,description,price,stock,categories_id) values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Jawaban 4 Mengambil Data dari Database
a. Mengambil data users
   select name, email from users;

b. Mengambil data items
   select name, description, stock from items where price >=1000000;
   select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
select items.name, items.description,items.price,items.stock,items.categories_id,categories.name as kategori from items inner join categories on items.categories_id=categories.id;
Jawaban 5 Mengubah Data dari Database
update items set price = 2500000 where name="sumsang b50";
